// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 1 || n == 2 {
        return 1;
    }
    let mut f1 = 1;
    let mut f2 = 1;
    for _i in 2..n {
        let temp = f1;
        f1 = f2;
        f2 += temp;
    }
    return f2;
}


fn main() {
    println!("{}", fibonacci_number(10));
}
