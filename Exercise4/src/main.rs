use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let tx1 = mpsc::Sender::clone(&tx);

    let handler1 = thread::spawn(move || {
        tx.send("Send from t1").unwrap();
        thread::sleep(Duration::from_millis(1000));
    });
    let handler2 = thread::spawn(move || {
        tx1.send("Send from t2").unwrap();
        thread::sleep(Duration::from_millis(1000));
    });

    for received in rx {
        println!("Got: {}", received);
    }

    handler1.join().unwrap();
    handler2.join().unwrap();
}
